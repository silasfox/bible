const button = document.getElementById('search');
var verses;
const reload = document.getElementById('reload');
const debug = document.getElementById('debug');
const input = document.getElementById('search-input');
const content = document.getElementById('content');
const counter = document.getElementById('counter');
const phrases = document.getElementById('phrases');

button.addEventListener('click', updateButton);
reload.addEventListener('click', reloadBible);
debug.addEventListener('click', debugBible);

var socket = new WebSocket('ws://localhost:20000/search');

socket.onopen = function () {
    console.log("Verbindung wurde erfolgreich aufgebaut");
};

var body = new Map();

socket.onmessage = function (messageEvent) {
    console.log(messageEvent.data);
    var json = JSON.parse(messageEvent.data);
    handleMessage(json);
    displayCounter(countOfVerses);
};

const verseToLi = verse => {
    return '<tr><td>' + verse.bsname + ' ' + verse.chapter + ',' + verse.number + '</td><td>' +
    verse.text.replace(verse.phrase, '<strong>' + verse.phrase + '</strong>') + '</td></tr>';
}

const displayCounter = async verses => {
    counter.innerHTML = verses  + ' Ergebnis' + (verses != 1 ? 'se' : '');
}

var countOfVerses = 0;

const handleVerse = async verse => {
    verses.innerHTML += verseToLi(verse);
    countOfVerses++;
}

var phrasesArray = [];

const handlePhrase = async phrase => {
    phrasesArray.push(phrase);
    phrases.innerHTML = '<p>' + phrasesArray.join(', ') + '</p>';
}

const handleMessage = async message => {
    if (typeof message === 'string') {
        handlePhrase(message);
    } else {
        handleVerse(message);
    }
}

socket.onerror = function (errorEvent) {
    console.log("Error! Die Verbindung wurde unerwartet geschlossen");
};

socket.onclose = function (closeEvent) {
    console.log('Die Verbindung wurde geschlossen --- Code: ' + closeEvent.code + ' --- Grund: ' + closeEvent.reason);
};

function updateButton() {
  content.innerHTML = '<table id="verses"></table>';
  verses = document.getElementById('verses');
  counter.innerHTML = '';
  phrases.innerHTML = '';
  phrasesArray = [];
  countOfVerses = 0;
  socket.send(JSON.stringify({phrase:input.value}));
}

function reloadBible() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:20000/reload/" + prompt("Which bible? (mng, kjv, vul, grb)"));
    xhr.send();
}

function debugBible() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:20000/debug");
    xhr.send();
}
