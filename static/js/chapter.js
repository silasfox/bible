const button = document.getElementById('book');
const reload = document.getElementById('reload');
const debug = document.getElementById('debug');
const input = document.getElementById('search-input');
const chapter = document.getElementById('chapter-input');
const content = document.getElementById('content');

button.addEventListener('click', updateButton);
reload.addEventListener('click', reloadBible);
debug.addEventListener('click', debugBible);

var socket = new WebSocket('ws://localhost:20000/chapter');

socket.onopen = function () {
    console.log("Verbindung wurde erfolgreich aufgebaut");
};

socket.onmessage = function (messageEvent) {
    console.log(messageEvent.data);
    var json = JSON.parse(messageEvent.data);
    handleMessage(json);
};

const verseToP = verse => {
    return '<p>' + verse.chapter + ':' + verse.number + ' ' + verse.text + '</p>';
}

const handleMessage = async message => {
    content.innerHTML += verseToP(message);
}

socket.onerror = function (errorEvent) {
    console.log("Error! Die Verbindung wurde unerwartet geschlossen");
};

socket.onclose = function (closeEvent) {
    console.log('Die Verbindung wurde geschlossen --- Code: ' + closeEvent.code + ' --- Grund: ' + closeEvent.reason);
};

function updateButton() {
  content.innerHTML = '';
  socket.send(JSON.stringify({book:input.value,chapter:Number(chapter.value)}));
}

function reloadBible() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:20000/reload/" + prompt("Which bible? (mng, kjv, vul, grb)"));
    xhr.send();
}

function debugBible() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:20000/debug");
    xhr.send();
}
