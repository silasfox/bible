use Bible::Backend::Data;
use Bible::Backend::Display;
use Bible::Backend::Logic;
use Cro::WebSocket::BodyParsers;
use Cro::WebSocket::BodySerializers;
use Cro::HTTP::Router;
use Cro::HTTP::Router::WebSocket;
use JSON::Fast;

my Bible $bible;

sub routes is export {
    route {
        get -> {
            static 'static/index.html'
        }
        get -> 'menge' {
            unless $bible.defined {
                $bible = Bible.new(:tsv-lines('resources/mng.tsv'.IO.lines))
            }
            static 'static/menge.html'
        }

        get -> 'menge-b' {
            unless $bible.defined {
                $bible = Bible.new(:tsv-lines('resources/mng.tsv'.IO.lines))
            }
            static 'static/book.html'
        }

        get -> 'menge-c' {
            unless $bible.defined {
                $bible = Bible.new(:tsv-lines('resources/mng.tsv'.IO.lines))
            }
            static 'static/chapter.html'
        }

        get -> 'static', *@path {
            static 'static/', @path
        }

        get -> 'js', *@path {
            static 'static/js', @path
        }

        get -> 'search' {
            web-socket :json, -> $incoming {
                supply whenever $incoming -> $message {
                    my $phrase = (await $message.body)<phrase>;
                    try my $channel = sem-search $bible.get-bible, $phrase;
                    while my $verse = $channel.receive {
                        emit $verse
                    }
		    emit False;
                }
            }
        }

        get -> 'book' {
            web-socket :json, -> $incoming {
                supply whenever $incoming -> $message {
                    my $book = (await $message.body)<book>;
                    try my $channel = find-book $bible.get-bible, $book;
                    while my $verse = $channel.receive {
                        emit $verse
                    }
                }
            }
        }

        get -> 'chapter'  {
            web-socket :json, -> $incoming {
                supply whenever $incoming -> $message {
                    my $json = await $message.body;
                    try my $channel = find-chapter-from-channel(find-book-logic($bible.get-bible, $json<book>),
                            $json<chapter>);
                    while my $verse = $channel.receive {
                        emit $verse
                    }
                }
            }
        }

	get -> 'reload' {
	    header('Access-Control-Allow-Origin', '*');
            $bible = Bible.new(:tsv-lines("resources/mng.tsv".IO.lines), :!debug)
	}

        get -> 'reload', $book {
            $bible = Bible.new(:tsv-lines("resources/$book.tsv".IO.lines), :!debug)
        }

        get -> 'debug' {
            $bible = Bible.new(:tsv-lines('resources/mng.tsv'.IO.lines), :debug)
        }

        get -> 'is-alive' {
	    header('Access-Control-Allow-Origin', '*');
	    content 'application/json', { :status('ok') }
        }
    }
}
