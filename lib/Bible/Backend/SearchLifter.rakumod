use Bible::Backend::Data;

constant small-words = <der die das des dem dessen deren ein er
den und ist von auf es sie wie so euch ihr eine sich in>;

sub normalize-string(Str $string is copy) is export is pure {
    $string ~~ s:g/\(<-[)]>*\)//;
    # TODO: rewrite regex so that (eig. something) is not removed
    $string ~~ s:g/<:P>//;
    return $string;
}

sub diff-strings(@strings, Str $phrase) is export is pure {
	(@strings.lazy».words «∖» $phrase).flat».keys.flat
}

sub remove-small-words(@words) is export is pure {
	(@words.lazy».&normalize-string)
	    .grep: -> $word { !($word ∈ small-words) }
}

sub tally(@words) is export is pure {
	my %freqs;
	@words.lazy».&{ %freqs{$_} += 1 }
	%freqs;
}

sub filter-by-frequency($elems, $channel, %freqs) is export {
    (%freqs.keys ==> sort { %freqs{$_} })».&{
        $channel.send: $_ if %freqs{$_} / $elems > 0.25
    }
    $channel.send: False
}

sub lift-search(Channel:D $channel, Str:D $phrase, @search) is export {
    $channel.send: $phrase;
    diff-strings @search, $phrase
            ==> remove-small-words()
	    ==> tally()
            ==> filter-by-frequency(@search.elems, $channel)
}
