class Verse is export {
    has Str:D $.bname = '';
    has Str:D $!bsname = '';
    has $!bnumber;
    has $.chapter;
    has $!number;
    has Str $.text = '';
    has Bool $.debug = False;

    proto method from-string(Str:D $string --> Verse) {
        say $string if $!debug;
        {*};
    }

    multi method from-string(Str:D $string --> Verse) {
        ($!bname, $!bsname, $!bnumber, $!chapter, $!number, $!text) = $string.split(/\t/);
        $!bnumber = +$!bnumber;
        $!chapter = +$!chapter;
        $!number = +$!number;
        return self;
    }

    method Str {
        "{ $!bname }\t{ $!bsname }\t{ $!bnumber }\t{ $!chapter }\t{ $!number }\t{ $!text }\n";
    }

    method to-hash($phrase) {
        { :$phrase, :$!bsname, :$!chapter, :$!number, :$!text }
    }
}

class Bible {
    has Str @.tsv-lines is required;
    has @!bible;
    has Bool $.debug;

    method !init-bible {
        @!bible = @!tsv-lines.lazy.map: -> Str $str { Verse.new(:$!debug).from-string($str) }
    }

    method get-bible {
        self!init-bible unless @!bible;
        return @!bible;
    }
}
