use Bible::Backend::Data;

class VerseDisplayer {
    has $.verse;

    method Str {
        return "{$!verse.book.name}\t{$!verse.book.sname}\t{$!verse.book.number}\t{$!verse.chapter.number}\t{$!verse.number}\t{$!verse.text}";
    }
}
