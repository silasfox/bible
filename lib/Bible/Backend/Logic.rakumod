use Bible::Backend::Data; use Bible::Backend::SearchLifter;

sub handle-verse(Verse:D $verse, Channel:D $channel, Str:D $phrase) is export {
    start {
        $channel.send: $verse.to-hash: $phrase
    }
}

sub filter(@bible, Str:D $phrase) is pure {
    @bible.lazy.grep: { normalize-string($_.text).contains: $phrase }
}

sub search(@bible, Str:D $phrase, Channel:D $channel) is export {
    await Promise.allof(
        filter(@bible, $phrase)».&{ handle-verse $_, $channel, $phrase });

    $channel.send: False
}

sub infix:<¦>(Channel:D $inner-channel, Channel:D $channel) {
    start {
	gather while my $verse = $inner-channel.receive {
	    $channel.send: take $verse
	}
    }
}

sub run-search(@bible, Str:D $phrase, Channel:D $channel) {
    my $inner-channel = Channel.new;
    start {
        search(@bible, $phrase, $inner-channel)
    }
    my $search = $inner-channel¦$channel;
    lift-search $channel, $phrase, (await $search).map: {$_<text>}
}

sub sem-search(@bible, Str:D $phrase) is export {
    my $channel = Channel.new;
    start {
        run-search @bible, $phrase, $channel;
    }
    return $channel;
}

sub find-book-logic(@bible, Str:D $book) is export {
    my $channel = Channel.new;
    start {
        @bible.grep(-> $verse { $verse.bname.contains: $book })».&{ $channel.send: $_ }
	$channel.send: False;
    }
    return $channel;
}

sub find-book(@bible, Str:D $book) is export {
    my $channel = Channel.new;
    start {
        react whenever find-book-logic(@bible, $book) -> $verse {
            $channel.send: $verse.to-hash: $book if $verse
        }
	$channel.send: False
    }
    return $channel
}

sub find-chapter-from-channel(Channel:D $channel, Int:D $chapter) is export {
    my $result = Channel.new;
    start {
        react whenever $channel -> $verse {
            $result.send: $verse.to-hash: "" if $verse && $verse.chapter == $chapter
        }
        $result.send: False
    }
    return $result
}
