use Cro::HTTP::Log::File;
use Cro::HTTP::Server;
use Bible::Routes;

my Cro::Service $http = Cro::HTTP::Server.new(http => <1.1>,
        host => %*ENV<BIBLE_HOST> || die("Missing BIBLE_HOST in environment"),
        port => %*ENV<BIBLE_PORT> || die("Missing BIBLE_PORT in environment"), application => routes(),
        after => [Cro::HTTP::Log::File.new(logs => $*OUT, errors => $*ERR)]);
$http.start;
say "Listening at http://%*ENV<BIBLE_HOST>:%*ENV<BIBLE_PORT>";
react {
    whenever signal(SIGINT) {
        say "Shutting down...";
        $http.stop;
        done;
    }
}
